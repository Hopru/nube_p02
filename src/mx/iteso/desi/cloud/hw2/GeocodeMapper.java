package mx.iteso.desi.cloud.hw2;

import mx.iteso.desi.cloud.Geocode;
import mx.iteso.desi.cloud.GeocodeWritable;
import mx.iteso.desi.cloud.ParseTriple;
import mx.iteso.desi.cloud.ParserCoordinates;
import mx.iteso.desi.cloud.Triple;

import org.apache.hadoop.mapreduce.*;

import java.io.IOException;

import org.apache.hadoop.io.*;

public class GeocodeMapper extends Mapper<LongWritable, Text, Text, GeocodeWritable> {

  /* TODO: Your mapper code here */
    private Geocode geocode;
    private Triple triple;
    private static Geocode philadelphia = new Geocode("philadelphia", 39.88, -75.25);
    private static Geocode houston = new Geocode("houston", 29.97, -95.35);
    private static Geocode seattle = new Geocode("seattle", 47.45, -122.30);
    private static Geocode guadalajara = new Geocode("guadalajara", 20.52, -103.31);
    private static Geocode monterrey = new Geocode("monterrey", 25.66, -100.3);

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    	
        triple = ParseTriple.parseTriple(value.toString());
        if(triple == null) {
    		return;
    	}
        if(triple.getPredicate().equalsIgnoreCase("http://xmlns.com/foaf/0.1/depiction")) {
          geocode = new Geocode(triple.getObject(), 0, 0);
          context.write(new Text(triple.getSubject()), new GeocodeWritable(geocode));
        } else if(triple.getPredicate().equalsIgnoreCase("http://www.georss.org/georss/point")){
        	Double coordinates[] = ParserCoordinates.parseCoordinates(triple.getObject());
          geocode = new Geocode("", coordinates[0].doubleValue(), coordinates[1].doubleValue());
          double distanceToPhiladelphia = philadelphia.getHaversineDistance(geocode.getLatitude(), geocode.getLongitude());
          double distanceToHouston = houston.getHaversineDistance(geocode.getLatitude(), geocode.getLongitude());
          double distanceToSeattle = seattle.getHaversineDistance(geocode.getLatitude(), geocode.getLongitude());
          double distanceToGuadalajara = guadalajara.getHaversineDistance(geocode.getLatitude(), geocode.getLongitude());
          double distanceToMonterrey = monterrey.getHaversineDistance(geocode.getLatitude(), geocode.getLongitude());
          if(distanceToPhiladelphia < 5000 || distanceToHouston < 5000 || distanceToSeattle < 5000 || distanceToGuadalajara < 5000 || distanceToMonterrey < 5000) {
            context.write(new Text(triple.getSubject()), new GeocodeWritable(geocode));
          }
        }
    }
}
