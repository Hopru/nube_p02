package mx.iteso.desi.cloud.hw2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import mx.iteso.desi.cloud.GeocodeWritable;

import org.apache.hadoop.mapreduce.Job;

public class GeocodeDriver {

  public static void main(String[] args) throws Exception {
  
    if (args.length != 3) {
      System.err.println("Usage: GeocodeDriver <input path> <output path>");
      System.exit(-1);
    }
    
    /* TODO: Your driver code here */
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "Geocoder");
    job.setJarByClass(GeocodeDriver.class);
    job.setMapperClass(GeocodeMapper.class);
    job.setReducerClass(GeocodeReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(GeocodeWritable.class);
    MultipleInputs.addInputPath(job, new Path(args[0]),TextInputFormat.class);
    MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class);
    MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class);
    FileOutputFormat.setOutputPath(job, new Path(args[2]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
