package mx.iteso.desi.cloud.hw2;

import org.apache.hadoop.mapreduce.*;

import java.io.IOException;

import org.apache.hadoop.io.*;
import mx.iteso.desi.cloud.Geocode;
import mx.iteso.desi.cloud.GeocodeWritable;

public class GeocodeReducer extends Reducer<Text, GeocodeWritable, Text, Text> {

  private Text result = new Text();

  /* TODO: Your reducer code here */
  public void reduce(Text key, Iterable<GeocodeWritable> values, Context context) throws IOException, InterruptedException {
    Geocode geocode;
    String name = "";
    double lat = 0;
    double lon = 0;
    for(GeocodeWritable v : values) {
        geocode = v.getGeocode();
        name += geocode.getName();
        lat += geocode.getLatitude();
        lon += geocode.getLongitude();
    }

    if(lat == 0 || lon == 0 || name.trim().equals("")) {
    	return;
    }
    geocode = new Geocode(name, lat, lon);
    result.set(geocode.toString());
    context.write(key, result);
}
}
